//==============================================================================
//    This file was auto-generated!
//==============================================================================
//==============================================================================

#include "MainComponent.h"

//==========================================================================
/** Constructor */
MainComponent::MainComponent (Audio& audio_, QueueManager& queueManager_) :	 queueManager(queueManager_), audio(audio_)
{
    //mainComponent====
    setSize (800, 600);
    intButtons();
    
    //queueGui==================
    addAndMakeVisible(queueGui);
}

/** Destructor */
MainComponent::~MainComponent()
{
    killButtons();
}

void MainComponent::resized()
{

}

void MainComponent::paint(Graphics& g)
{
	queueGui.paint(g);
}

//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", "Cues", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    //add menu items.
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    if (topLevelMenuIndex == 1)
        menu.addItem(NewAudioCue, "New Audio Cue", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    //FileMenuCode====================
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }

    //QuesMenuCode=============================================================================
    if (topLevelMenuIndex == cueMenu)
    {
		if (menuItemID == NewAudioCue)
		{
			DBG("MainComponent::NewAudioCue");

			loadAudioFile();
		}
    }
}

//ButtonCode====================================================================
void MainComponent::buttonClicked(Button* button)
{
	if (button == &goButton)
	{
		queueManager.playNextCue();
	}
    
	else if (button == &stopButton)
	{
		queueManager.stopAllCues();
	}

	else if (button == &fadeButton)
	{
        queueManager.stopAllCues();
	}
}

void MainComponent::intButtons()
{
    //Prep start button.
    goButton.setBounds(5, 5, 50, 50);
    goButton.addListener(this);
    goButton.setButtonText("GO");
    goButton.setColour(TextButton::buttonColourId, Colours::green);
    addAndMakeVisible(goButton);
    
    //Prep stop button.
    stopButton.setBounds(5, 60, 50, 50);
    stopButton.addListener(this);
    stopButton.setButtonText("STOP");
    stopButton.setColour(TextButton::buttonColourId, Colours::red);
    addAndMakeVisible(stopButton);

	//Prep fade button.
	//fadeButton.setBounds(5, 115, 50, 50);
	//fadeButton.addListener(this);
	//fadeButton.setButtonText("FADE");
	//fadeButton.setColour(TextButton::buttonColourId, Colours::blue);
	//addAndMakeVisible(fadeButton);
}

void MainComponent::killButtons()
{
    //Remove button listeners.
    goButton.removeListener(this);
	stopButton.removeListener(this);
	//fadeButton.removeListener(this);
}

void MainComponent::loadAudioFile()
{
	FileChooser myChooser("Please select the moose you want to load...",
		File::getSpecialLocation(File::userHomeDirectory),
		"*.wav");
	if (myChooser.browseForFileToOpen())
	{
		File mooseFile(myChooser.getResult());
		DBG("pre queue manager");
		DBG(myChooser.getResult().getFullPathName());
		queueManager.addFile(mooseFile);
	}
}