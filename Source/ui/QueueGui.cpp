#include "QueueGui.h"

QueueGui::QueueGui()
{
	addAndMakeVisible(queueListGui);
	queueListGui.setBounds(55, 5, 200, 500);
	queueListGui.setColour(ListBox::outlineColourId, Colours::black);
	queueListGui.setOutlineThickness(1);
	queueListGui.setModel(this);

	numRows = 5;
}

QueueGui::~QueueGui()
{

}

//TableListBoxModel==============================================================================================
int QueueGui::getNumRows()
{
	return numRows;
}

void QueueGui::paintCell(Graphics& g, int rowNumber, int columnId, int width, int height, bool /*rowIsSelected*/)
{
	g.setColour(Colours::black);
	g.setFont(font);

	
		const String text("Hello World");

		g.drawText(text, 2, 0, width - 4, height, Justification::centredLeft, true);
	

	g.setColour(Colours::black.withAlpha(0.2f));
	g.fillRect(width - 1, 0, 1, height);
}

void QueueGui::paintRowBackground(Graphics& g, int rowNumber, int /*width*/, int /*height*/, bool rowIsSelected)
{
	if (rowIsSelected)
		g.fillAll(Colours::lightblue);
	else if (rowNumber % 2)
		g.fillAll(Colour(0xffeeeeee));
}