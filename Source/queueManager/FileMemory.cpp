#include "FileMemory.h"

FileMemory::FileMemory()
{
	//InitiateVariables=====================
	memorySize = 0;
	dynamicFileArray = nullptr;
}

FileMemory::~FileMemory()
{
	//Remove File Memory=======
	delete[] dynamicFileArray;
	dynamicFileArray = nullptr;
}

void FileMemory::newFile(const File& newFile)
{
	DBG("FileMemory::NewFile");
	//create temp file location.
	File* tempFileArray;
	tempFileArray = new File[memorySize + 1];

	if (memorySize != 0)
	{
		//Copy to new file location.
		for (int i = 0; i < memorySize; i++)
		{
			memcpy(&tempFileArray[i], &dynamicFileArray[i], sizeof(&tempFileArray));
		}
	}

	//input new value to memory.
	tempFileArray[memorySize] = newFile;

	//delete old array and asign temp array to new array.
	if (memorySize != 0)
		delete[] dynamicFileArray;
	
	dynamicFileArray = tempFileArray;
	tempFileArray = nullptr;

	//inc array size index.
	memorySize++;
}

void FileMemory::removeFile(int fileIndex)
{
	DBG("FileMemory::RemoveFile");
	//create temporary pointer and new array.
	int i[2] = { 0,0 };
	File* tempFileArray = new File[memorySize - 1];

	//copy old array values to new array. Skipping unwanted value.
	for (i[0] = 0; i[0] <= memorySize; i[0]++, i[1]++)
	{
		if (i[0] != fileIndex) {
			tempFileArray[i[0]] = dynamicFileArray[i[1]];
		}
		else {
			i[1]++;
			tempFileArray[i[0]] = dynamicFileArray[i[1]];
		}

	}

	//reduce memory size.
	memorySize--;

	//delete old array and assign new array to class pointer(for the array).
	delete[] dynamicFileArray;
	dynamicFileArray = tempFileArray;
	tempFileArray = nullptr;
}

void FileMemory::editFile(File& newFile, int fileIndex)
{
	DBG("FileMemory::EditFile");

	//create temp file location.
	File* tempFileArray = new File[memorySize];

	//Copy to new file location.
	for (int i = 0; i < memorySize; i++)
	{
		if (i != fileIndex)
			memcpy(&tempFileArray[i], dynamicFileArray, sizeof(&tempFileArray));
		else if ((i = fileIndex))
			memcpy(&tempFileArray[i], &newFile, sizeof(&tempFileArray));
	}

	//delete old array and asign temp array to new array.
	delete[] dynamicFileArray;
	dynamicFileArray = tempFileArray;
	tempFileArray = nullptr;
}

File& FileMemory::getFile(int fileIndex)
{
	DBG("FileMemory::GetFile");

	//return requested array value.
	return dynamicFileArray[fileIndex];
}