#ifndef H_FILEMEMORY
#define H_FILEMEMORY

#include "../JuceLibraryCode/JuceHeader.h"

class FileMemory : public Component
{
public:
	FileMemory();
	~FileMemory();

	void FileMemory::newFile(const File& newFile);
	void FileMemory::removeFile(int fileIndex);
	void editFile(File& newFile, int fileIndex);
	File& getFile(int fileIndex);

private:
	File* dynamicFileArray;

	int memorySize;
};

#endif