#include "QueueManager.h"

QueueManager::QueueManager(FilePlayerManager& filePlayerManager_)	:	filePlayerManager(filePlayerManager_)
{
	queueIndex = 0;
	queuePlaybackIndex = 0;
}

QueueManager::~QueueManager()
{

}

//transport======================================================
void QueueManager::playNextCue(void)
{
	if (queuePlaybackIndex < queueIndex)
	{
		filePlayerManager.playNextCue(queueList.getReference(queuePlaybackIndex));
		queuePlaybackIndex++;
	}
}

void QueueManager::stopAllCues(void)
{
	filePlayerManager.stopAllCues();
}

//fileArrayFunctions====================
void QueueManager::addFile(const File newFile)
{
	DBG("preaddfile");
	queueList.add(newFile);
	queueIndex++;
}
