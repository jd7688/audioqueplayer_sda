#ifndef H_QUEUEMANAGER
#define H_QUEUEMANAGER

#include "../JuceLibraryCode/JuceHeader.h"
#include "../audio/FilePlayerManager.h"
#include "FileMemory.h"

class QueueManager : public Component
{
public:
	QueueManager(FilePlayerManager& filePlayerManager_);
	~QueueManager();

	//transport============
	void playNextCue(void);
	void stopAllCues(void);

	//fileArrayFunctions==============
	void addFile(const File newFile);

private:
	FilePlayerManager& filePlayerManager;

	Array<File> queueList;
	//FileMemory queueList;

	int queueIndex;
	int queuePlaybackIndex;
};

#endif