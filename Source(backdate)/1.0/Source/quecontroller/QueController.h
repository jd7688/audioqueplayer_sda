//
//  QueController.h
//  JuceBasicAudio
//
//  Created by Joseph D'Souza on 16/01/2017.
//
//

#ifndef QueController_h
#define QueController_h

#include "../audio/Audio.h"
#include "../../JuceLibraryCode/JuceHeader.h"
//#include "QueMemory.h"
#include "../ui/FilePlayerGui.h"

class QueController :   public Component
{
public:
    //mainstuff=========================================
    QueController(Audio& audio_);
    ~QueController();
    
    //queIndex Controls==================================
    int getQueIndex();
    void setQueIndex(int newIndex);
    void incQueIndex();
    
    //queIndex Controls==================================
    int getPlaybackIndex();
    void setPlaybackIndex(int newIndex);
    void incPlaybackIndex();
    
    //playcues===========================================
    void playNextCue(File fileToPlay);
    void stopAllCues();
    void fadeAllCues();
    
    //Audio==============================================
    AudioDeviceManager& getAudioDeviceManager();
    
    //AudioQueFunctions==================================
    void newAudioQue(void);
    void removeAudioQue(void);
    //    AudioQuePlayerUI* getQue (int index);
    int size(void);
    
    //Gui================================================
    void resized();
    
private:
    int queIndex;
    int playbackIndex;
    int audioQuePlayerUIArrayIndex;
    
    ScopedPointer<Audio> audio;
//    QueMemory queMemory;
    
    FilePlayerGui audioQuePlayerUI;
};

#endif /* QueController_h */
