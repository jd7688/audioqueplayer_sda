/*
  ==============================================================================

    Audio.h
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#ifndef AUDIO_H_INCLUDED
#define AUDIO_H_INCLUDED

#define noAudioPlayers 8

/**
 Class containing all audio processes
 */

#include "../../JuceLibraryCode/JuceHeader.h"
#include "FilePlayer.h"
//#include "../quecontroller/QueController.h"

class Audio :   public MidiInputCallback,
                public AudioIODeviceCallback
{
public:
    /** Constructor */
    Audio();
    
    /** Destructor */
    ~Audio();
    
    /** Returns the audio device manager, don't keep a copy of it! */
    AudioDeviceManager& getAudioDeviceManager() { return audioDeviceManager;}
    
    FilePlayer& getFilePlayer(int i) { return filePlayer[i]; }
    
    void handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message) override;
    
    void audioDeviceIOCallback (const float** inputChannelData,
                                int numInputChannels,
                                float** outputChannelData,
                                int numOutputChannels,
                                int numSamples) override;
    
    void audioDeviceAboutToStart (AudioIODevice* device) override;
    
    void audioDeviceStopped() override;
    
    //AudioQuePlayers=====================================
    int getAvaliblePlayer();
    
    int getNoAudioPlayers() {return noAudioPlayersPortal;}
    
private:
    AudioDeviceManager audioDeviceManager;
    MixerAudioSource audioSourceMixer;
    AudioSourcePlayer audioSourcePlayer;
    FilePlayer filePlayer[noAudioPlayers];
    
    int noAudioPlayersPortal;
};



#endif  // AUDIO_H_INCLUDED
