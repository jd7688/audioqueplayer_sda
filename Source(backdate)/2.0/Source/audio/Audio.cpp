/*
  ==============================================================================

    Audio.cpp
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#include "Audio.h"

Audio::Audio()
{
    noAudioPlayersPortal = noAudioPlayers;
    
    audioDeviceManager.initialiseWithDefaultDevices (0, 2); //2 inputs, 2 outputs
    
    for(int i = 0; i< noAudioPlayers; i++)
    {
        audioSourceMixer.addInputSource(&filePlayer[i], true);
    }
    
    //load the filePlayer into the audio source
    audioSourcePlayer.setSource (&audioSourceMixer);
    
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);
}

Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
    audioSourceMixer.removeAllInputs();
}


void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    //All MIDI inputs arrive here
}

void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)
{
    // get the audio from our file player - player puts samples in the output buffer
    audioSourcePlayer.audioDeviceIOCallback (inputChannelData, numInputChannels, outputChannelData, numOutputChannels, numSamples);
    
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
//    float inSampL;
//    float inSampR;
    
    while(numSamples--)
    {
//        inSampL = *outL;
//        inSampR = *outL;
        
//        *outL = inSampL * 1.f;
//        *outR = inSampR * 1.f;
        
        inL++;
        inR++;
        outL++;
        outR++;
    }
}


void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    audioSourcePlayer.audioDeviceAboutToStart (device);
}

void Audio::audioDeviceStopped()
{
    audioSourcePlayer.audioDeviceStopped();
}

//AudioQuePlayers===============================================================
int Audio::getAvaliblePlayer()
{
    bool test = false;
    int i = 0;
    
    do
    {
        test = filePlayer[i].getState();
        i++;
        
    }while(test == false && i < noAudioPlayers);
    
    if(test == false)
        return -1;
    else if(test == true)
        return i;
}

