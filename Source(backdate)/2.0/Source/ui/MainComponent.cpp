/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent (Audio& audio_) : audio (audio_)/*, filePlayerGui (audio_.getFilePlayer(0))*/, queController(audio_)
{
    setSize (500, 400);
    addAndMakeVisible(filePlayerGui);
    
    intButtons();
}

MainComponent::~MainComponent()
{
    killButtons();
}

void MainComponent::resized()
{
    queController.resized();
    filePlayerGui.setBounds (0, 0, getWidth(), 20);
}

//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}


//ButtonCode====================================================================
void MainComponent::buttonClicked(Button* button)
{
    if(button == &goButton)
        queController.playNextCue(filePlayerGui.getSavedFile());
    
    else if(button == &stopButton)
        queController.stopAllCues();
}

void MainComponent::intButtons()
{
    //Prep start button.
    goButton.setBounds(40, 40, 40, 40);
    goButton.addListener(this);
    goButton.setButtonText("GO");
    goButton.setColour(TextButton::buttonColourId, Colours::green);
    addAndMakeVisible(goButton);
    
    //Prep stop button.
    stopButton.setBounds(40, 100, 40, 40);
    stopButton.addListener(this);
    stopButton.setButtonText("STOP");
    stopButton.setColour(TextButton::buttonColourId, Colours::red);
    addAndMakeVisible(stopButton);
}

void MainComponent::killButtons()
{
    goButton.removeListener(this);
    stopButton.removeListener(this);
}

