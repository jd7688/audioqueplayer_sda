//==============================================================================
//    This file was auto-generated!
//==============================================================================
//==============================================================================

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"
#include "../audio/Audio.h"
#include "FilePlayerGui.h"
#include "../quecontroller/QueController.h"


//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent   : public Component,
                        public MenuBarModel,
                        public Button::Listener
{
public:
    //==========================================================================
    /** Constructor */
    MainComponent (Audio& audio_);

    /** Destructor */
    ~MainComponent();

    void resized() override;
    
    //MenuBarEnums/Callbacks====================================================
    //Menus.
    enum Menus
    {
        FileMenu=0,
        queMenu=1,
        
        NumMenus
    };
    
    //File menu items.
    enum FileMenuItems
    {
        AudioPrefs = 1,
        
        NumFileItems
    };
    
    //Que menu items.
    enum QueMenuItems
    {
        NewAudioQue = 1,
        
        NumQueItems
    };
    
    StringArray getMenuBarNames() override;
    PopupMenu getMenuForIndex (int topLevelMenuIndex, const String& menuName) override;
    void menuItemSelected (int menuItemID, int topLevelMenuIndex) override;
    
    //ButtonCode================================================================
    void buttonClicked(Button* button) override;
    void intButtons();
    void killButtons();
    
private:
    Audio& audio;
    FilePlayerGui filePlayerGui;
    
    TextButton goButton;
    TextButton stopButton;
    
    QueController queController;
    

    //==========================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};


#endif  // MAINCOMPONENT_H_INCLUDED
