//==============================================================================
//    This file was auto-generated!
//==============================================================================
//==============================================================================

#include "MainComponent.h"

//==========================================================================
/** Constructor */
MainComponent::MainComponent (Audio& audio_) : audio (audio_)/*, filePlayerGui (audio_.getFilePlayer(0))*/, queController(audio_)
{
    setSize (800, 600);
    addAndMakeVisible(filePlayerGui);
    
    intButtons();
}

/** Destructor */
MainComponent::~MainComponent()
{
    killButtons();
}

void MainComponent::resized()
{
    queController.resized();
//    filePlayerGui.setBounds (0, 0, getWidth(), 20);
}

//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", "Cues", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    //add menu items.
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    if (topLevelMenuIndex == 1)
        menu.addItem(NewAudioQue, "New Audio Cue", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    //FileMenuCode====================
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }

    //QuesMenuCode====================
    if (topLevelMenuIndex == queMenu)
    {
        if (menuItemID == NewAudioQue)
        {
            DBG("MainComponent::NewAudioQue");
            queController.newAudioQue();
            
            FilePlayerGui* newFilePlayerUI = &queController.getQuePlayerUI(queController.getQueIndex());
            newFilePlayerUI->setBounds (0, 0, getWidth(), 20);
            addAndMakeVisible(newFilePlayerUI);
        }
    }
}

//ButtonCode====================================================================
void MainComponent::buttonClicked(Button* button)
{
    if(button == &goButton)
        queController.playNextCue(filePlayerGui.getSavedFile());
    
    else if(button == &stopButton)
        queController.stopAllCues();
}

void MainComponent::intButtons()
{
    //Prep start button.
    goButton.setBounds(40, 40, 40, 40);
    goButton.addListener(this);
    goButton.setButtonText("GO");
    goButton.setColour(TextButton::buttonColourId, Colours::green);
    addAndMakeVisible(goButton);
    
    //Prep stop button.
    stopButton.setBounds(40, 100, 40, 40);
    stopButton.addListener(this);
    stopButton.setButtonText("STOP");
    stopButton.setColour(TextButton::buttonColourId, Colours::red);
    addAndMakeVisible(stopButton);
}

void MainComponent::killButtons()
{
    //Remove button listeners.
    goButton.removeListener(this);
    stopButton.removeListener(this);
}

