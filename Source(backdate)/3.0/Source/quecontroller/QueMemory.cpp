//==============================================================================
//  QueMemory.cpp
//  JuceBasicAudio
//
//  Created by Joseph D'Souza on 05/01/2017.
//==============================================================================

#include "QueMemory.h"

//Main======================================
QueMemory::QueMemory()
{
    //InitiateVariables=====================
    memorySize = 0;
    dynamicFileArray = new File[memorySize];
	filePlayerUIArray = new FilePlayerGui[memorySize];
}

QueMemory::~QueMemory()
{
    //Remove File Memory======================
    delete[] dynamicFileArray;
	delete[] filePlayerUIArray;
    dynamicFileArray = nullptr;
	filePlayerUIArray = nullptr;
}

void QueMemory::newFile(const File& newFile)
{
    DBG("QueMemory::NewFile");
    //create temp file location.
    File* tempFileArray = new File[memorySize+1];
    
    //Copy to new file location.
    for (int i = 0; i < memorySize; i++)
    {
        memcpy(&tempFileArray[i], dynamicFileArray, sizeof(&tempFileArray));
    }
    
    //increment array and input new value to memory.
    memorySize++;
    tempFileArray[memorySize-1] = newFile;
    
    //delete old array and asign temp array to new array.
    delete[] dynamicFileArray;
    dynamicFileArray = tempFileArray;
    tempFileArray = nullptr;
}

void QueMemory::removeFile(int fileIndex)
{
    DBG("QueMemory::RemoveFile");
    //create temporary pointer and new array.
    int i[2] = {0,0};
    File* tempFileArray = new File[memorySize-1];
    
    //copy old array values to new array. Skipping unwanted value.
    for(i[0] = 0; i[0] <= memorySize; i[0]++, i[1]++)
    {
        if(i[0] != fileIndex){
            tempFileArray[i[0]] = dynamicFileArray[i[1]];
        }
        else{
            i[1]++;
            tempFileArray[i[0]] = dynamicFileArray[i[1]];
        }
        
    }
    
    //reduce memory size.
    memorySize--;
    
    //delete old array and assign new array to class pointer(for the array).
    delete[] dynamicFileArray;
    dynamicFileArray = tempFileArray;
    tempFileArray = nullptr;
}

void QueMemory::editFile(File& newFile, int fileIndex)
{
    DBG("QueMemory::EditFile");
    
    //create temp file location.
    File* tempFileArray = new File[memorySize];
    
    //Copy to new file location.
    for (int i = 0; i < memorySize; i++)
    {
        if (i != fileIndex)
            memcpy(&tempFileArray[i], dynamicFileArray, sizeof(&tempFileArray));
        else if ((i = fileIndex))
            memcpy(&tempFileArray[i], &newFile, sizeof(&tempFileArray));
    }
    
    //delete old array and asign temp array to new array.
    delete[] dynamicFileArray;
    dynamicFileArray = tempFileArray;
    tempFileArray = nullptr;
}

File& QueMemory::getFile(int fileIndex)
{
    DBG("QueMemory::GetFile");

    //return requested array value.
    return dynamicFileArray[fileIndex];
}

void QueMemory::newQuePlayerUI()
{
	DBG("QueMemory::NewQuePlayerUI(Start)");
	//create temp file location.
	ScopedPointer<AudioQuePlayerUI> tempUIArray = new AudioQuePlayerUI[memorySize];
	int i;

	//Copy to new file location.
	for (i = 0; i < memorySize; i++)
	{
        DBG("QueMemory::NewQuePlayerUI(memcopy1)");
		memcpy((void*)&tempUIArray[i], (void*)audioQuePlayerUIArray, sizeof(&tempUIArray));
        DBG("QueMemory::NewQuePlayerUI(memcopy2)");
	}

	/*input new value to memory.
	i++;
	memcpy((void*)&tempUIArray[i], (void*)&newQuePlayer, sizeof(tempUIArray));
    */

	//delete old array and asign temp array to new array.
	delete[] audioQuePlayerUIArray;
    DBG("QueMemory::NewQuePlayerUI(deleted)");
	audioQuePlayerUIArray = tempUIArray;
    DBG("QueMemory::NewQuePlayerUI(End)");
}

void QueMemory::removeQuePlayerUI(int quePlayerIndex)
{
	DBG("QueMemory::RemoveQuePlayerUI");
	//create temporary pointer and new array.
	int i[2] = { 0,0 };
	AudioQuePlayerUI* tempUIArray = new AudioQuePlayerUI[memorySize - 1];

	//copy old array values to new array. Skipping unwanted value.
	for (i[0] = 0; i[0] <= memorySize; i[0]++, i[1]++)
	{
		if (i[0] != quePlayerIndex) 
		{
			memcpy((void*)&tempUIArray[i[0]], (void*)&audioQuePlayerUIArray[i[1]], sizeof(tempUIArray));
		}

		else 
		{
			i[1]++;
			memcpy((void*)&tempUIArray[i[0]], (void*)&audioQuePlayerUIArray[i[1]], sizeof(tempUIArray));
		}
	}

	//delete old array and assign new array to class pointer(for the array).
	delete[] audioQuePlayerUIArray;
	audioQuePlayerUIArray = tempUIArray;
	tempUIArray = nullptr;
}

AudioQuePlayerUI& QueMemory::getQuePlayerUI(int quePlayerIndex)
{
	DBG("QueMemory::GetQuePlayer");

	//return requested array value.
	return audioQuePlayerUIArray[quePlayerIndex];
}

int QueMemory::getMemorySize()
{
    DBG("QueMemory::GetMemorySize");
    return memorySize;
}
//==============================================================================
//==============================================================================