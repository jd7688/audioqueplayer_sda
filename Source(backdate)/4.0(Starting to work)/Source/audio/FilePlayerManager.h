#ifndef H_FILEPLAYERMANAGER
#define H_FILEPLAYERMANAGER

#include "../JuceLibraryCode/JuceHeader.h"
#include "FilePlayer.h"

#define noAudioPlayers 8

class FilePlayerManager : public Component
{
public:
	FilePlayerManager();
	~FilePlayerManager();

	MixerAudioSource& getAudioSourceMixer(void);

	//AudioFilePlayers==========
	int getAvaliblePlayer(void);

	//Transport=======================
	void playNextCue(File& fileToPlay);
	void stopAllCues(void);

private:
	FilePlayer filePlayer[noAudioPlayers];
	MixerAudioSource audioSourceMixer;
};

#endif