#include "FilePlayerManager.h"

FilePlayerManager::FilePlayerManager()
{
	for (int i = 0; i < noAudioPlayers; i++)
	{
		audioSourceMixer.addInputSource(&filePlayer[i], true);
	}
    
    DBG("FilePlayerManager::Constructor");
}

FilePlayerManager::~FilePlayerManager()
{
	audioSourceMixer.removeAllInputs();
	audioSourceMixer.releaseResources();
}

MixerAudioSource& FilePlayerManager::getAudioSourceMixer(void)
{
	return audioSourceMixer;
}

//AudioFilePlayers===============================================================
int FilePlayerManager::getAvaliblePlayer(void)
{
	//create temp testing values.
	int i = 0;
	bool test = false;

	//test for avalible players.
	do
	{
		test = filePlayer[i].getState();
		i++;

	}while(test == false && i < noAudioPlayers);

	//return player index if avalible.
	if(test == true)
		return i;
    
	//return -1 if no player avalible.
	else
        return -1;
}

//Transport=========================================
void FilePlayerManager::playNextCue(File& fileToPlay)
{
	DBG("FilePlayerManager::playNextCue");
	//get avalible filePlayer index.
	int player = getAvaliblePlayer();
	//int player = 0;
	
	//prepare fileplayer and set playing.
	if (player != -1)
	{
        filePlayer[player].setState(false);
		filePlayer[player].loadFile(fileToPlay);
		filePlayer[player].setPlaying(true);
	}
}

void FilePlayerManager::stopAllCues(void)
{
	//stop all players and make avalible for use.
	for (int i = 0; i < noAudioPlayers; i++)
	{
		filePlayer[i].setPlaying(false);
		filePlayer[i].setState(true);
	}
}