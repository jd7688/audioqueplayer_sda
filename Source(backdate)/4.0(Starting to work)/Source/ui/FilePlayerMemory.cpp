//==============================================================================
//  FilePlayerMemory.cpp
//  JuceBasicAudio
//
//  Created by Joseph D'Souza on 17/01/2017.
//==============================================================================
//==============================================================================

#include "FilePlayerMemory.h"

FileMemory::FileMemory()
{
    memorySize = 0;
}

FileMemory::~FileMemory()
{
    delete[] filePlayerUIArray;
}

void FileMemory::newQuePlayerUI()
{
    DBG("QueMemory::NewQuePlayerUI(Start)");
    //create temp file location.
    ScopedPointer<FilePlayerGui> tempUIArray = new FilePlayerGui[memorySize];
    int i;
    
    //Copy to new file location.
    for (i = 0; i < memorySize; i++)
    {
        DBG("QueMemory::NewQuePlayerUI(memcopy1)");
        memcpy((void*)&tempUIArray[i], (void*)filePlayerUIArray, sizeof(&tempUIArray));
        DBG("QueMemory::NewQuePlayerUI(memcopy2)");
    }
    
    //input new value to memory.
    i++;
    memcpy((void*)&tempUIArray[i], (void*)new FilePlayerGui, sizeof(tempUIArray));
    
    
    //delete old array and asign temp array to new array.
    delete[] filePlayerUIArray;
    memorySize++;
    DBG("QueMemory::NewQuePlayerUI(deleted)");
    filePlayerUIArray = tempUIArray;
    DBG("QueMemory::NewQuePlayerUI(End)");
}

void FileMemory::removeQuePlayerUI(int quePlayerIndex)
{
    DBG("QueMemory::RemoveQuePlayerUI");
    //create temporary pointer and new array.
    int i[2] = { 0,0 };
    FilePlayerGui* tempUIArray = new FilePlayerGui[memorySize - 1];
    
    //copy old array values to new array. Skipping unwanted value.
    for (i[0] = 0; i[0] <= memorySize; i[0]++, i[1]++)
    {
        if (i[0] != quePlayerIndex)
        {
            memcpy((void*)&tempUIArray[i[0]], (void*)&filePlayerUIArray[i[1]], sizeof(tempUIArray));
        }
        
        else
        {
            i[1]++;
            memcpy((void*)&tempUIArray[i[0]], (void*)&filePlayerUIArray[i[1]], sizeof(tempUIArray));
        }
    }
    
    //delete old array and assign new array to class pointer(for the array).
    delete[] filePlayerUIArray;
    memorySize--;
    filePlayerUIArray = tempUIArray;
    tempUIArray = nullptr;
}

FilePlayerGui& FileMemory::getQuePlayerUI(int quePlayerIndex)
{
    //return requested array value.
    DBG("QueMemory::GetQuePlayer");
    
    return filePlayerUIArray[quePlayerIndex];
    DBG("QueMemory::GetQuePlayer(Done)");
}

int FileMemory::getMemorySize()
{
    DBG("QueMemory::GetMemorySize");
    return memorySize;
}