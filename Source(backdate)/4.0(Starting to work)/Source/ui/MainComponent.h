//==============================================================================
//    This file was auto-generated!
//==============================================================================
//==============================================================================

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"
#include "../audio/Audio.h"
#include "QueueGui.h"
#include "../queueManager/QueueManager.h"

//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent   : public Component,
                        public MenuBarModel,
                        public Button::Listener
{
public:
    //==========================================================================
    /** Constructor */
    MainComponent (Audio& audio_, QueueManager& queueManager_);

    /** Destructor */
    ~MainComponent();

    void resized() override;
    
    //MenuBarEnums/Callbacks====================================================
    //Menus.
    enum Menus
    {
        FileMenu=0,
        cueMenu=1,
        
        NumMenus
    };
    
    //File menu items.
    enum FileMenuItems
    {
        AudioPrefs = 1,
        
        NumFileItems
    };
    
    //Que menu items.
    enum CueMenuItems
    {
        NewAudioCue = 1,
        
        NumCueItems
    };
    
    StringArray getMenuBarNames() override;
    PopupMenu getMenuForIndex (int topLevelMenuIndex, const String& menuName) override;
    void menuItemSelected (int menuItemID, int topLevelMenuIndex) override;
    
    //ButtonCode================================================================
    void buttonClicked(Button* button) override;
    void intButtons();
    void killButtons();

	void loadAudioFile();
    
private:
	QueueManager& queueManager;
	Audio& audio;
    
    TextButton goButton;
    TextButton stopButton;
	TextButton fadeButton;
    
    QueueGui queueGui;

    //==========================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};


#endif  // MAINCOMPONENT_H_INCLUDED
