//==============================================================================
//  FilePlayerMemory.h
//  JuceBasicAudio
//
//  Created by Joseph D'Souza on 17/01/2017.
//==============================================================================
//==============================================================================

#ifndef FilePlayerMemory_h
#define FilePlayerMemory_h

#include "../../JuceLibraryCode/JuceHeader.h"
#include "FilePlayerGui.h"

class FileMemory    :   public Component
{
public:
    FileMemory();
    ~FileMemory();
    
    void newQuePlayerUI();
    void removeQuePlayerUI(int quePlayerIndex);
    FilePlayerGui& getQuePlayerUI(int quePlayerIndex);
    
    int getMemorySize();
    
    
private:
    ScopedPointer<FilePlayerGui> filePlayerUIArray;

	std::vector<FilePlayerGui> array;
    
    int memorySize;
};


#endif /* FilePlayerMemory_h */
