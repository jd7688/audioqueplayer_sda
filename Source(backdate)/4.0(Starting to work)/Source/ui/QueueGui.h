#ifndef H_QUEUEGUI
#define H_QUEUEGUI

#include "../JuceLibraryCode/JuceHeader.h"

class QueueGui : public Component,
				 public TableListBoxModel
{
public:
	QueueGui();
	~QueueGui();

	//TableListBoxModel==============================================================================================
	int getNumRows() override;
	void paintCell(Graphics& g, int rowNumber, int columnId, int width, int height, bool /*rowIsSelected*/) override;
	void paintRowBackground(Graphics& g, int rowNumber, int /*width*/, int /*height*/, bool rowIsSelected) override;


private:
	TableListBox queueListGui;
	Font font;

	int numRows;

};


#endif