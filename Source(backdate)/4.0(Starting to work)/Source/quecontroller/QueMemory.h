//
//  QueMemory.h
//  JuceBasicAudio
//
//  Created by Joseph D'Souza on 05/01/2017.
//
//

#ifndef QueMemory_h
#define QueMemory_h

#include "../../JuceLibraryCode/JuceHeader.h"
#include "../ui/FilePlayerGui.h"

class QueMemory
{
public:
    //Main================================
    QueMemory();
    ~QueMemory();
    
    //Functions===========================
    void newFile(const File& newFile);
    void removeFile(int fileIndex);
    void editFile(File& newFile, int fileIndex);
    File& getFile(int fileIndex);

	void newQuePlayerUI();
	void removeQuePlayerUI(int quePlayerIndex);
//	AudioQuePlayerUI& getQuePlayerUI(int quePlayerIndex);

    int getMemorySize();
    
private:
    int memorySize;
    File* dynamicFileArray;
	ScopedPointer<FilePlayerGui> filePlayerUIArray;
};


#endif /* QueMemory_h */