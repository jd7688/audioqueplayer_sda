//==============================================================================
//  QueController.cpp
//  JuceBasicAudio
//
//  Created by Joseph D'Souza on 04/01/2017.
//==============================================================================
//==============================================================================

#include "QueController.h"

//mainstuff=====================================================================
QueController::QueController(Audio& audio_)	: audio(&audio_)
{
    DBG("QueController::Constuctor");
    queIndex = 0;
    playbackIndex = 0;
    audioQuePlayerUIArrayIndex = queIndex;
}

QueController::~QueController()
{
    DBG("QueController::Destuctor");
    stopAllCues();
}

//queIndex Controls==================================
int QueController::getQueIndex(){return queIndex;}
void QueController::setQueIndex(int newIndex){queIndex = newIndex;}
void QueController::incQueIndex(){queIndex++;}

//queIndex Controls==================================
int QueController::getPlaybackIndex(){return playbackIndex;}
void QueController::setPlaybackIndex(int newIndex){playbackIndex = newIndex;}
void QueController::incPlaybackIndex(){playbackIndex++;}

//playcues======================================================================
void QueController::playNextCue(File fileToPlay)
{
    //gets next avalible FilePlayer and plays next que in requested player.
	DBG("QueController::PlayNextCue");
    int player;
    player = audio->getAvaliblePlayer();
    
    if(player != -1)
    {
        audio->getFilePlayer(player).loadFile(fileToPlay);
        audio->getFilePlayer(player).setPlaying(true);
    }
}

void QueController::stopAllCues()
{
    DBG("QueController::StopAllCues");
    
    //Stop all players.
    for(int i = 0; i < audio->getNoAudioPlayers(); i++)
    {
        audio->getFilePlayer(i).setPlaying(false);
    }
}

void QueController::fadeAllCues()
{
    DBG("QueController::FadeAllCues");
    
    //Call after fade complete.
    stopAllCues();
}

//Audio==============================================
AudioDeviceManager& QueController::getAudioDeviceManager()
{
    return getAudioDeviceManager();
}

//AudioQueFunctions=============================================================
void QueController::newAudioQue(void)
{
    DBG("QueController::NewAudioQue");
    
//    FilePlayerGui* newQuePlayerUI = new FilePlayerGui;
    
    fileMemory.newQuePlayerUI();
    incQueIndex();
}

void QueController::removeAudioQue(void)
{
    DBG("QueController::RemoveAudioQue");
}


int QueController::size(void)
{
    DBG("QueController::Size");
    return NULL;
}

//Gui================================================
void QueController::resized()
{
    DBG("QueController::resized");
}

//QueMem=============================================
FilePlayerGui& QueController::getQuePlayerUI(int queIndex)
{
    return fileMemory.getQuePlayerUI(queIndex);
}


//==============================================================================
//==============================================================================