#include "QueueManager.h"

QueueManager::QueueManager(FilePlayerManager& filePlayerManager_)	:	filePlayerManager(filePlayerManager_)
{
	queueIndex = 0;
	queuePlaybackIndex = 0;
}

QueueManager::~QueueManager()
{

}

//transport======================================================
void QueueManager::playNextCue(void)
{
	filePlayerManager.playNextCue(queueList.getReference(queuePlaybackIndex));
	queuePlaybackIndex++;
}

void QueueManager::stopAllCues(void)
{
	filePlayerManager.stopAllCues();
}

//fileArrayFunctions====================
void QueueManager::addFile(const File newFile)
{
	DBG("preaddfile");
	queueList.add(newFile);
}

/*File& QueueManager::loadAudioFile()
{
	FileChooser myChooser("Please select the moose you want to load...",
		File::getSpecialLocation(File::userHomeDirectory),
		"*.wav");
	if (myChooser.browseForFileToOpen())
	{
		File mooseFile(myChooser.getResult());
		DBG("pre queue manager");
		queueManager.addFile(mooseFile);
	}
}*/